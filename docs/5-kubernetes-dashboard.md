# 五、kubernetes dashboard用户界面安装使用

## 1. 下载kubernetes-dashboard.yaml文件
通过：https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml 地址，能够下载下载kubernetes-dashboard.yaml文件。

 


## 2. 编辑kubernetes-dashboard.yaml文件
通过编辑工具打开kubernetes-dashboard.yaml，并在此文件中的Service部分下添加type: NodePort和nodePort: 30001，添加位置如下图所示。
![img](https://toocruel-ghost.oss-cn-hangzhou.aliyuncs.com//content/images/2018/11/QQ20181114-225541@2x.png?x-oss-process=style/style1)
##### 因为被墙，需要替换k8s.gcr.io下的镜像地址，如下图所示：
![](https://toocruel-ghost.oss-cn-hangzhou.aliyuncs.com//content/images/2018/11/QQ20181114-230658@2x.png?x-oss-process=style/style1)

将kubernetes-dashboard-amd64 镜像替换为国内镜像，这里用我自己做的，其实就是翻墙后下载重新上传，地址为：registry.cn-shanghai.aliyuncs.com/9tong-base/kubernetes-dashboard-amd64

修改后的效果，如下图：
![](https://toocruel-ghost.oss-cn-hangzhou.aliyuncs.com//content/images/2018/11/QQ20181114-231119@2x.png?x-oss-process=style/style1)

这里提供我改后的文件地址：[https://gitee.com/toocruel/kubernetes-starter/raw/master/kuernetes-dashboard/kubernetes-dashboard.yaml](https://gitee.com/toocruel/kubernetes-starter/raw/master/kuernetes-dashboard/kubernetes-dashboard.yaml)

## 3. 通过执行 `kubectl create` 命令部署Web UI
```aidl
kubectl create -f  {path}/kubernetes-dashboard.yaml
```
如果你直接用我编辑好的文件也可以，更简单了
```aidl
kubectl create -f  https://gitee.com/toocruel/kubernetes-starter/raw/master/kuernetes-dashboard/kubernetes-dashboard.yaml
```

## 4. 访问Dashboard用户界面
在Dashboard部署完成后，就可以访问服务。
在浏览器中输入:https://{Master IP}:30001，此处为：https://106.14.46.249:30001/，打开页面如下：
注意，会提示证书不可信，要点信任：
![](https://toocruel-ghost.oss-cn-hangzhou.aliyuncs.com//content/images/2018/11/QQ20181114-232538@2x.png?x-oss-process=style/style1)

## 5. 创建一个管理员用户
创建amind-user.yaml文件，文件内容如下：
```aidl
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system

---

apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin-user
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
```
也可以直接使用网络文件：[https://gitee.com/toocruel/kubernetes-starter/raw/master/kuernetes-dashboard/amind-user.yaml](https://gitee.com/toocruel/kubernetes-starter/raw/master/kuernetes-dashboard/amind-user.yaml)

通过执行如下命令创建admin-user：
```aidl
kubectl create -f {path}/admin-user.yaml
```
也可以
```aidl
kubectl create -f https://gitee.com/toocruel/kubernetes-starter/raw/master/kuernetes-dashboard/amind-user.yaml

```

## 6. 获取管理员用户的Token
通过执行如下命令获取系统Token信息：
```aidl
kubectl describe  secret admin-user --namespace=kube-system
```
![](https://toocruel-ghost.oss-cn-hangzhou.aliyuncs.com//content/images/2018/11/QQ20181114-233915@2x.png?x-oss-process=style/style1)
使用该token已经可以登录，接下来是配置文件的方式
## 7. 添加Token至kubeconfig文件
通过编辑工具打开kubeconfig文件(~/.kube/config)，并添加Token
![](https://toocruel-ghost.oss-cn-hangzhou.aliyuncs.com//content/images/2018/11/QQ20181114-235305@2x.png?x-oss-process=style/style1)
## 8. 导入kubeconfig文件
在界面中导入kubeconfig文件。

## 9. 使用Dashboard

在默认情况下，Dashboard显示默认(default)命名空间下的对象，也可以通过命名空间选择器选择其他的命名空间。在Dashboard用户界面中能够显示集群大部分的对象类型。

## Dashboard提供的功能

- 集群管理
    
    集群管理视图用于对节点、命名空间、持久化存储卷、角色和存储类进行管理。 节点视图显示CPU和内存的使用情况，以及此节点的创建时间和运行状态。 命名空间视图会显示集群中存在哪些命名空间，以及这些命名空间的运行状态。角色视图以列表形式展示集群中存在哪些角色，这些角色的类型和所在的命名空间。 持久化存储卷以列表的方式进行展示，可以看到每一个持久化存储卷的存储总量、访问模式、使用状态等信息；管理员也能够删除和编辑持久化存储卷的YAML文件。
- 工作负载
    
    工作负载视图显示部署、副本集、有状态副本集等所有的工作负载类型。在此视图中，各种工作负载会按照各自的类型进行组织。 工作负载的详细信息视图能够显示应用的详细信息和状态信息，以及对象之间的关系。
    
- 服务发现和负载均衡
    
    服务发现视图能够将集群内容的服务暴露给集群外的应用，集群内外的应用可以通过暴露的服务调用应用，外部的应用使用外部的端点，内部的应用使用内部端点。
    
- 存储

    存储视图显示被应用用来存储数据的持久化存储卷申明资源。
    
- 配置

    配置视图显示集群中应用运行时所使用配置信息，Kubernetes提供了配置字典（ConfigMaps）和秘密字典（Secrets），通过配置视图，能够编辑和管理配置对象，以及查看隐藏的敏感信息。
    
- 日志视图

    Pod列表和详细信息页面提供了查看日志视图的链接，通过日志视图不但能够查看Pod的日志信息，也能够查看Pod容器的日志信息。通过Dashboard能够根据向导创建和部署一个容器化的应用，当然也可以通过手工的方式输入指定应用信息，或者通过上传YAML和JSON文件来创建和不受应用。

##### 手动创建应用
通过向导创建和部署容器化应用时，需要提供如下的一些信息：

- 应用名称（App name 必需）： 需要部署的应用的名称。带有此值的标签将会被添加至部署和服务中。在当前的Kubernetes命名空间中，应用名称必须是唯一的。同时，应用名称必须以小写字母开头，以小写字母和数字结尾，可以包含字母、数字和“-”。名称最长为24个字母。

- 容器组个数（Number of pods 必需）： 希望部署的容器组数量。值必须为整数。

- 描述（Description）： 对于应用的描述，将被添加至部署的注释中，并在应用详细信息中显示。

- 标签（Labels）： 应用的默认标签为应用的名称和版本。可以指定其它的标签，这些标签将会被应用至部署、服务、容器组等资源中。

- 命名空间（Namespace）：在同一个物理集群中，Kubernetes支持多个虚拟集群。这些虚拟集群被称为命名空间，通过命名空间可以将资源进行逻辑上的划分。通过下列菜单可以选择已有的命名空间，当然也可以创建新的命名空间。命名空间的名称最大的字符数为63，名词可以使用字母、数字“-”，不能包含大写字母，同时也不能全部使用数字。

- 镜像拉取保密字典（Image Pull Secret）： 如果Docker容器镜像是私有的，则有可能需要保密证书。Dashboard通过下拉菜单提供了所有的可用的保密凭证，也允许创建新的保密字典。保密字典名称必须遵循DNS域名语法，例如：new.image-pull.secret。保密字典的内容必须使用基于base64进行加密的，并在.dockercfg文件中进行指定。保密字典名称最长不能超过253个字符。

- 环境变量（Environment variables）： Kubernetes通过环境变量暴露服务， 可以创建环境变量或者使用环境变量的值将参数传递给命令。环境变量能够被应用用来发现服务，环境变量的值可以通过￥（VAR_NAME）语法被其它变量引用。

##### 上传YAML或JSON文件创建应用
通过编译工具编写容器化应用的YAML和JSON文件，在Dashboard用户界面中通过上传文件创建和部署应用。

## 10. 参考材料
- 《Web UI (Dashboard)》地址：https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
 
